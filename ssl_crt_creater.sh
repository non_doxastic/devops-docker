#!/bin/bash

country=MOR
state=capital
locality=Rabat
organization=Breakout.net
organizationalunit=IT
email=admin@breakout.net


checkRoot() {
   if [ $(id -u) -ne 0 ]; then
     printf "Script must be run as root. Try 'sudo ./ssl_certificate_creater.sh'\n"
     exit 1
   fi
}


setServerName() {
    choices=breakout.test
    if [ "$choices" != "" ]; then
        __servername=$choices
    else
        break
    fi
}


installCertificateApache() {
  mkdir -p /etc/haproxy/cert
 
 openssl req -new -x509 -days 365 -nodes -out /etc/haproxy/cert/$__servername.crt -keyout /etc/haproxy/cert/$__servername.key -subj '/CN=breakout.test/O=Breakout test company./C=US'


  
  chmod 600 /etc/haproxy/cert/$__servername.key
  chmod 600 /etc/haproxy/cert/$__servername.crt
}


checkRoot
setServerName
installCertificateApache



exit 0
