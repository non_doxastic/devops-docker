FROM oraclelinux
MAINTAINER Youssef BOUKHDIMI <academik.hits@gmail.com>

#Adding and Updating Packages
RUN yum -y update && yum clean all
RUN yum -y install httpd && yum clean all
RUN yum -y install mod_ssl openssl crypto-utils
RUN yum -y install haproxy
#RUN yum install -y vim
#RUN yum install -y net-tools

#Simple startup script to aviod some issues observed with container restart
ADD run-httpd.sh /.
ADD ssl_crt_creater.sh /.
RUN chmod -v +x /run-httpd.sh
RUN chmod -v +x /ssl_crt_creater.sh

RUN mkdir -p /var/www/html/Breakout/logs
RUN mkdir /etc/httpd/sites-available
RUN mkdir /etc/httpd/sites-enabled
RUN mkdir -p /etc/apache2/ssl
RUN mkdir -p /etc/haproxy/cert

#Copy config file across
COPY ./breakout.test.conf /etc/httpd/sites-available
COPY ./Breakout/ /var/www/html/Breakout/
COPY ./haproxy.local /etc/haproxy
COPY ./hosts /.

#Modify web apache server
RUN ln -s /etc/httpd/sites-available/breakout.test.conf /etc/httpd/sites-enabled/breakout.test.conf
RUN echo "IncludeOptional sites-enabled/*.conf" >> /etc/httpd/conf/httpd.conf
RUN sed -i 's/Listen 80/Listen 8000/g' /etc/httpd/conf/httpd.conf
RUN sed -i 's/Listen 443 https/#Listen 443 https/g' /etc/httpd/conf.d/ssl.conf

RUN ls -lrt /
RUN cat ./hosts >> /etc/hosts

#Add HTTPS Support to HaProxy
RUN ./ssl_crt_creater.sh
RUN cat /etc/haproxy/cert/breakout.test.crt /etc/haproxy/cert/breakout.test.key > /etc/haproxy/cert/breakout.test.pem


CMD /usr/sbin/haproxy -D -f /etc/haproxy/haproxy.local -p /var/run/haproxy.pid && /usr/sbin/httpd -D FOREGROUND

EXPOSE 80 443
