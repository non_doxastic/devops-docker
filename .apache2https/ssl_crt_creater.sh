#!/bin/bash

country=MOR
state=capital
locality=Rabat
organization=Breakout.net
organizationalunit=IT
email=admin@breakout.net


checkRoot() {
   if [ $(id -u) -ne 0 ]; then
     printf "Script must be run as root. Try 'sudo ./ssl_certificate_creater.sh'\n"
     exit 1
   fi
}


setServerName() {
    choices=breakout.test
    if [ "$choices" != "" ]; then
        __servername=$choices
    else
        break
    fi
}


installCertificateApache() {
  mkdir -p /etc/apache2/ssl

########
  #openssl genrsa -des3 -passout pass:x -out keypair.key 2048
  #openssl rsa -passin pass:x -in keypair.key -out /etc/apache2/ssl/$__servername.key
 # openssl req -new -key /etc/apache2/ssl/$__servername.key -out /etc/apache2/ssl/$__servername.csr \
  #  -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=breakout.test/emailAddress=$email"
  #openssl x509 -req -days 365 -in /etc/apache2/ssl/$__servername.csr -signkey /etc/apache2/ssl/$__servername.key -out /etc/apache2/ssl/$__servername.crt

########

 openssl req -new -x509 -days 365 -nodes -out /etc/apache2/ssl/$__servername.crt -keyout /etc/apache2/ssl/$__servername.key -subj '/CN=breakout.test/O=Breakout test company./C=US'

  
  chmod 600 /etc/apache2/ssl/$__servername.key
  chmod 600 /etc/apache2/ssl/$__servername.crt
}


checkRoot
setServerName
installCertificateApache


exit 0
